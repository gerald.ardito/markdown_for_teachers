# Markdown for Teachers

This project in GitLab is a playground in which to explore how Markdown can be used by teachers to create create and share curricular materials with one another and with their students.

## GitLab and Markdown

[Markdown](https://www.markdownguide.org/getting-started/) is a lightweight (meaning easy to use without much investment of time) markup language (a tool for creating beautiful formatted documents from essentially plain text documents).

[GitLab](https://gitlab.com) is tool usually used by software developers to store and share and keep track of different versions of software. 
For our purposes, we are going to consider curricular plans and materials as the equivalent of software. You can store anything (any kind of digital file - markdown, pictures, pdfs, etc.) in a GitLab repository or project.

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## Create a GitLab Account and Setting up Your First Project (Repository)

1. **Go to https://gitlab.com/ and set up a new account**. You can either use an email/password or connect use your Google or Twitter account.

![](https://gitlab.com/gerald.ardito/markdown_for_teachers/-/raw/main/images/GitLab_Home.png)

2. **Create a Project**. Fill in the fields on this form. And under Visibility Level, select "Public." For example, a project could be a content area (11th grade US Histor), a unit, a project, whatever.

![](https://gitlab.com/gerald.ardito/markdown_for_teachers/-/raw/main/images/GitLab_Project_Name.png)

3. **Add files to your project.** You can upload files (including Markdown files) to your project. To do this, go to your project in GitLab, then click on "Upload,"
![](https://gitlab.com/gerald.ardito/markdown_for_teachers/-/raw/main/images/upload_files.png)

## Example
I have set up [this GitLab project](https://gitlab.com/gerald.ardito/markdown_for_teachers) as an example. Just click on any of the documents in this project to see them.

Explore. Try. Break things and then fix them.

## Possible Workflow

```mermaid
graph TD
    A[GitLab Projects For Teachers]
    style A fill:#f9f,stroke:#333,stroke-width:4px
    A --> B(Create Content)
    B --> C(Upload Content to GitLab)
    C --> D(Share Project with Students)
    style B fill:#ccf,stroke:#f66,stroke-width:2px,stroke-dasharray: 5, 5
    style C fill:#ccf,stroke:#f66,stroke-width:2px,stroke-dasharray: 5, 5
    style D fill:#ccf,stroke:#f66,stroke-width:2px,stroke-dasharray: 5, 5
```
  

